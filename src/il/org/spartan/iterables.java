/** Part of the "Spartan Blog"; mutate the rest / but leave this line as is */
package il.org.spartan;

import static il.org.spartan.__.*;
import static il.org.spartan.azzert.*;

import java.util.*;

import org.eclipse.jdt.annotation.*;
import org.junit.*;
import org.junit.runners.*;

public enum iterables {
  // No values in an 'enum' used as name space for a collection of 'static'
  // functions.
  ;
  /** Counts the number of items in an {@link Iterable}.
   * @param <T> some arbitrary type
   * @param ts some iterable over items whose type is the type parameter
   * @return the number of items the given iterable yields. */
  public static <T> int count(final @Nullable Iterable<T> ts) {
    int $ = 0;
    if (ts != null)
      for (final @Nullable T t : ts)
        $ += as.bit(t != null);
    return $;
  }
  /** returns */
  /** @param <T> some arbitrary type
   * @return the empty iterator over values of the type parameter */
  public static <T> PureIterable.Sized<T> empty() {
    return as.iterable();
  }
  /** @param os some arbitrary iterable
   * @return <b><code>true</code><b> iff there are no non-null values in the
   *         parameter */
  public static boolean isEmpty(final Iterable<?> os) {
    for (final Iterator<?> i = os.iterator(); i.hasNext();)
      if (i.next() != null)
        return false;
    return true;
  }
  public static <T> PureIterable.Sized<T> singleton(final T t) {
    return as.iterable(t);
  }
  public static <T> PureIterator<T> singletonIterator(final T t) {
    return singleton(t).iterator();
  }

  /** A static nested class hosting unit tests for the nesting class Unit test
   * for the containing class. Note the naming convention: a) names of test
   * methods do not use are not prefixed by "test". This prefix is redundant. b)
   * test methods begin with the name of the method they check.
   * @author Yossi Gil
   * @since 2014-05-31 */
  @FixMethodOrder(MethodSorters.NAME_ASCENDING) //
  @SuppressWarnings({ "static-method", "javadoc" }) //
  public static class TEST {
    @Test public void containsDegenerate() {
      azzert.nay(contains("Hello"));
    }
    @Test public void containseturnsFalseTypical() {
      azzert.nay(contains("Hello", null, "x", "y", null, "z", "w", "u", "v"));
    }
    @Test public void containsSimple() {
      azzert.aye(contains("Hello", "e"));
    }
    @Test public void containsTypical() {
      azzert.aye(contains("Hello", "a", "b", "c", "d", "e", "f"));
    }
    @Test public void containsWithNulls() {
      azzert.aye(contains("Hello", null, "a", "b", null, "c", "d", "e", "f", null));
    }
    @Test public void countDoesNotIncludeNull() {
      azzert.that(count(as.iterable(null, "One", null, "Two", null, "Three")), is(3));
    }
    @Test public void countEmpty() {
      azzert.that(count(iterables.<String> empty()), is(0));
    }
    @Test public void countSingleton() {
      azzert.that(count(singleton(new Object())), is(1));
    }
    @Test public void countThree() {
      azzert.that(count(as.iterable("One", "Two", "Three")), is(3));
    }
  }
}
